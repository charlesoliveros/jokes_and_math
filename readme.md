# Jokes and Math Project

This project allows you to create, update and delete jokes in a local
database, also It allows you to get a random joke from this local database
or from two external APIs by using a Api Proxy Pattern.

## Requirements

* python 3.8+
* Postgres DataBase

## Installation

* Create env vars:
    * DB_HOST
    * DB_NAME
    * DB_USER
    * DB_PASSWORD
    * API_URL_CHUCK="https://api.chucknorris.io/jokes/random"
    * API_URL_DAD="https://icanhazdadjoke.com/"

* Install requirements:
    ```sh
    pip install -r requirements.txt
    ```

* Make sure you have a postgres database working.

* Run migrations:
    ```sh
    python manage.py migrate
    ```

## Run the project
* Make sure you are in the project's root and execute:
    ```sh
        python manage.py runserver
    ```
* Open the next URL in  your browser: http://127.0.0.1:8000/

* OpenAPI Documentacion with swagger: http://127.0.0.1:8000/api/v1/


# Design Patter Implemented

For this project was used the Proxy Design Pattern. A proxy class was 
implemented to comunicate our local project with the Chuck Nurris API 
and the DadJoke API. 
There is a litle explanation about this Pattern Design in a repository 
of my personal gitlab account:
https://gitlab.com/charlesoliveros/pydesignpatterns/-/blob/master/2_structural/proxy.py