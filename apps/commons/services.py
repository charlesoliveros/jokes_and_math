from abc import ABC, abstractmethod


class ServiceAbstrac(ABC):

    @abstractmethod
    def execute(cls) -> dict:
        ...
