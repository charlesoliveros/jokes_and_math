from abc import ABC, abstractmethod
import requests


class APIProxyAbstract(ABC):
    '''
    Proxy Pattern Design.
    Proxy class to be implemented
    '''

    api_url: str = None

    @abstractmethod
    def send_get_request(self) -> requests.Response:
        '''
        This method must be implemented
        '''
        pass
