from typing import Optional, List
from apps.commons.services import ServiceAbstrac
from .exceptions import (
    PathParamNumberIsNotValidAPIException,
    PathParamNumbersIsNotValidAPIException,
)


class MathOperationIncrementService(ServiceAbstrac):

    @classmethod
    def execute(cls, number: int = None) -> Optional[int]:
        try:
            number = int(number)
        except (ValueError, TypeError):
            raise PathParamNumberIsNotValidAPIException()

        number += 1
        return number


class MathOperationMCMService(ServiceAbstrac):

    @classmethod
    def validate_numbers(cls, numbers: List) -> Optional[List[int]]:
        try:
            return [int(number) for number in numbers]
        except (ValueError, TypeError):
            raise PathParamNumbersIsNotValidAPIException()

    @classmethod
    def execute(cls, numbers: list) -> Optional[int]:
        """
        Calcula el mínimo común múltiplo de una lista de números
        """
        numbers = cls.validate_numbers(numbers)

        # Encuentra el número máximo en la lista
        max_num = max(numbers)

        # Inicializa el múltiplo común más pequeño como el número máximo
        lcm = max_num

        # Itera sobre cada número en la lista
        for num in numbers:
            # Encuentra el siguiente múltiplo común del número actual
            # y el múltiplo común actual
            while lcm % num != 0:
                lcm += max_num

        return lcm
