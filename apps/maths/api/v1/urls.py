from django.urls import path
from .views import MathOperationAPIView


URL_PREFIX: str = 'math'

urlpatterns = [
    path(
        URL_PREFIX,
        MathOperationAPIView.as_view(),
        name=MathOperationAPIView.name
    ),
]
