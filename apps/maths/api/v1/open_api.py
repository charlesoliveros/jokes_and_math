from drf_yasg import openapi


class MathOperationQueryParams:
    number = openapi.Parameter(
        'number', in_=openapi.IN_QUERY, type=openapi.TYPE_INTEGER
    )
    numbers = openapi.Parameter(
        'numbers', in_=openapi.IN_QUERY, type=openapi.TYPE_STRING
    )
