from http import HTTPStatus
from rest_framework.reverse import reverse
from django.test import TestCase
from apps.maths.api.v1.views import MathOperationAPIView


class MathOperationAPIViewIntegrationTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.endpoint = reverse(MathOperationAPIView.name)

    def test_increment_returns_ok(self):
        data = {
            'number': 1,
        }
        response = self.client.get(self.endpoint, data=data)

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(response.json().get('answer'), 2)

    def test_increment_returns_bad_request(self):
        data = {
            'number': 'is not a number',
        }
        response = self.client.get(self.endpoint, data=data)

        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)

    def test_calcule_mcm_returns_ok(self):
        data = {
            'numbers': '1,2,3',
        }
        response = self.client.get(self.endpoint, data=data)

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(response.json().get('answer'), 6)

    def test_calcule_mcm_returns_bad_request(self):
        data = {
            'numbers': '1,2,A',
        }
        response = self.client.get(self.endpoint, data=data)

        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)
