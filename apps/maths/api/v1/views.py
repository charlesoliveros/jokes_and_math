from rest_framework.views import APIView
from rest_framework.response import Response
from drf_yasg.utils import swagger_auto_schema
from .services import MathOperationIncrementService, MathOperationMCMService
from .open_api import MathOperationQueryParams


class MathOperationAPIView(APIView):

    name: str = "math_operation_api_view"

    @swagger_auto_schema(
        manual_parameters=[
            MathOperationQueryParams.number,
            MathOperationQueryParams.numbers,
        ],
    )
    def get(self, request, format=None):
        response_data: dict = {"answer": None}
        numbers = request.query_params.get("numbers")
        number = request.query_params.get("number")

        if numbers:
            response_data['answer'] = MathOperationMCMService.execute(
                numbers=numbers.replace(" ", "").split(",")
            )
        elif number:
            response_data['answer'] = MathOperationIncrementService.execute(
                number=number
            )

        return Response(response_data)
