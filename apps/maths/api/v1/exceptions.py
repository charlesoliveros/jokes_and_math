from rest_framework.exceptions import APIException
from rest_framework.status import HTTP_400_BAD_REQUEST


class PathParamNumberIsNotValidAPIException(APIException):
    status_code = HTTP_400_BAD_REQUEST
    default_detail = (
        'Path param "number" has not a valid Value. '
        'It must be an integer.'
    )
    default_code = 'Invalid'


class PathParamNumbersIsNotValidAPIException(APIException):
    status_code = HTTP_400_BAD_REQUEST
    default_detail = (
        'Path param "numbers" has not a valid Value. '
        'All the values must be integers.'
    )
    default_code = 'Invalid'
