from django.conf import settings
import requests
from apps.commons.api_proxy import APIProxyAbstract


class ChuckAPIProxy(APIProxyAbstract):

    api_url: str = settings.API_URL_CHUCK

    @classmethod
    def send_get_request(cls) -> dict:
        response = requests.get(url=cls.api_url)
        response_json = response.json()
        return {
            "id": response_json.get("id"),
            "joke": response_json.get("value")
        }


class DadAPIroxy(APIProxyAbstract):

    api_url: str = settings.API_URL_DAD

    @classmethod
    def send_get_request(cls) -> dict:
        response = requests.get(
            url=cls.api_url, headers={"Accept": "application/json"}
        )
        response_json = response.json()
        return {
            "id": response_json.get("id"),
            "joke": response_json.get("joke")
        }
