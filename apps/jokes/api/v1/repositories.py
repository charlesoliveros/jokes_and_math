from typing import List, Optional
from django.db.models.query import QuerySet
from apps.jokes.models import Joke


class JokeRepo:

    @classmethod
    def get_all(cls) -> Optional[QuerySet]:
        return Joke.objects.all()

    @classmethod
    def get_all_ids(cls) -> List[Optional[int]]:
        return cls.get_all().values_list('id', flat=True)

    def get_joke_by_id(joke_id: int) -> Optional[Joke]:
        return Joke.objects.filter(id=joke_id).first()
