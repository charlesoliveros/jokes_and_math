from rest_framework.exceptions import APIException
from rest_framework.status import HTTP_400_BAD_REQUEST
from .constants import ALLOWED_NAMES


class PathParamNameIsNotValidAPIException(APIException):
    status_code = HTTP_400_BAD_REQUEST
    default_detail = (
        'Path param "Name" has not a valid Value. '
        f'Allowed values: {ALLOWED_NAMES}'
    )
    default_code = 'Invalid'
