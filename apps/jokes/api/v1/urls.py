from django.urls import path
from .views import JokeGetRandomCreateAPIView, JokeRetrieveUpdateDestroyAPIView


URL_PREFIX: str = 'jokes'

urlpatterns = [
    path(
        URL_PREFIX,
        JokeGetRandomCreateAPIView.as_view(),
        name=JokeGetRandomCreateAPIView.name
    ),
    path(
        URL_PREFIX,
        JokeRetrieveUpdateDestroyAPIView.as_view(),
        name=JokeRetrieveUpdateDestroyAPIView.name
    ),
    path(
        f'{URL_PREFIX}/<str:number>',
        JokeRetrieveUpdateDestroyAPIView.as_view(),
        name=JokeRetrieveUpdateDestroyAPIView.name
    ),
]
