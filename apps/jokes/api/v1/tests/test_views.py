from http import HTTPStatus
from unittest.mock import patch
from rest_framework.reverse import reverse
from django.test import TestCase
from apps.jokes.models import Joke
from apps.jokes.api.v1.constants import NAME_CHUCK, NAME_DAD
from apps.jokes.api.v1.views import (
    JokeGetRandomCreateAPIView, JokeRetrieveUpdateDestroyAPIView
)


class CreateAPIViewIntegrationTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.endpoint = reverse(JokeGetRandomCreateAPIView.name)

    def test_create_joke_returns_ok(self):
        data = {
            'joke': 'My Joke text',
        }
        response = self.client.post(self.endpoint, data=data)

        self.assertEqual(response.status_code, HTTPStatus.CREATED)


class GetRandomJokeAPIViewsIntegrationTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.endpoint = reverse(JokeGetRandomCreateAPIView.name)

    def test_get_random_joke_returns_ok_and_empty(self):
        response = self.client.get(self.endpoint)

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertEqual(response.json(), {'joke': ''})

    def test_get_random_joke_returns_ok_and_random_joke(self):
        joke1 = Joke.objects.create(joke='My Joke 1')
        joke2 = Joke.objects.create(joke='My Joke 2')

        response = self.client.get(self.endpoint)
        response_json = response.json()

        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertIn(response_json.get('id'), [joke1.id, joke2.id, ])


class RetrieveUpdateDestroyAPIViewIntegrationTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.endpoint = reverse(JokeRetrieveUpdateDestroyAPIView.name)
        cls.data = {'joke': 'new joke', }

    def test_update_joke_returns_not_found(self):
        NOT_FOUND_JOKE_ID = 100
        response = self.client.put(
            f'{self.endpoint}/{NOT_FOUND_JOKE_ID}', data=self.data
        )

        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)

    def test_update_joke_returns_ok(self):
        joke1 = Joke.objects.create(joke='My Joke 1')
        response = self.client.put(
            f'{self.endpoint}/{joke1.id}', data=self.data,  format='json'
        )

        self.assertEqual(
            response.status_code, HTTPStatus.UNSUPPORTED_MEDIA_TYPE
        )

    def test_delete_joke_returns_not_found(self):
        NOT_FOUND_JOKE_ID = 100
        response = self.client.delete(
            f'{self.endpoint}/{NOT_FOUND_JOKE_ID}', data=self.data
        )

        self.assertEqual(response.status_code, HTTPStatus.NOT_FOUND)

    def test_delete_joke_returns_ok(self):
        joke1 = Joke.objects.create(joke='My Joke 1')
        response = self.client.delete(
            f'{self.endpoint}/{joke1.id}', data=self.data
        )

        self.assertEqual(response.status_code, HTTPStatus.NO_CONTENT)

    def test_get_external_joke_with_api_proxy_returns_bad_request(self):
        INCORRECT_NAME = 'INCORRECT_NAME'
        response = self.client.get(
            f'{self.endpoint}/{INCORRECT_NAME}'
        )
        self.assertEqual(response.status_code, HTTPStatus.BAD_REQUEST)

    @patch('apps.jokes.api.v1.api_proxy.ChuckAPIProxy.send_get_request')
    def test_get_external_joke_with_api_proxy_returns_chuck_joke_ok(
        self, send_get_request_mocked
    ):
        send_get_request_mocked.return_value = {
            "id": "uZAJe96-SJWXscs2Vh2KEw",
            "joke": "Sasquatch brings Chuck Norris Jack Links beef jerky."
        }
        response = self.client.get(
            f'{self.endpoint}/{NAME_CHUCK}'
        )
        self.assertEqual(response.status_code, HTTPStatus.OK)
        send_get_request_mocked.assert_called()

    @patch('apps.jokes.api.v1.api_proxy.DadAPIroxy.send_get_request')
    def test_get_external_joke_with_api_proxy_returns_dad_joke_ok(
        self, send_get_request_mocked
    ):
        send_get_request_mocked.return_value = {
            "id": "0DdaxAX0orc",
            "joke": "I accidentally took my cats meds last night."
        }
        response = self.client.get(
            f'{self.endpoint}/{NAME_DAD}'
        )
        self.assertEqual(response.status_code, HTTPStatus.OK)
        send_get_request_mocked.assert_called()
