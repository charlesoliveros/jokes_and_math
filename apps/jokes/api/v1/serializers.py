from rest_framework import serializers
from apps.jokes.models import Joke


class JokeListCreateModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Joke
        fields = "__all__"


class JokeRetrieveUpdateDestroyModelSerializer(JokeListCreateModelSerializer):
    ...


class JokeSerializer(serializers.Serializer):
    id = serializers.CharField()
    joke = serializers.CharField()
