from typing import Union
from django.db.models.query import QuerySet
from rest_framework.response import Response
from rest_framework.generics import (
    ListCreateAPIView, RetrieveUpdateDestroyAPIView
)
from .serializers import (
    JokeListCreateModelSerializer, JokeRetrieveUpdateDestroyModelSerializer,
    JokeSerializer,
)
from .repositories import JokeRepo
from .services import JokeRetrieveService


class JokeGetRandomCreateAPIView(ListCreateAPIView):
    name: str = 'joke_create_api_view'
    serializer_class = JokeListCreateModelSerializer
    queryset: QuerySet = JokeRepo.get_all()

    def list(self, request, *args, **kwargs):
        serializer = self.get_serializer(JokeRetrieveService.execute())
        return Response(serializer.data)


class JokeRetrieveUpdateDestroyAPIView(RetrieveUpdateDestroyAPIView):
    name: str = 'joke_retrieve_update_destroy_api_view'
    serializer_class = JokeRetrieveUpdateDestroyModelSerializer
    lookup_url_kwarg: str = "number"
    lookup_field: str = "pk"
    queryset: QuerySet = JokeRepo.get_all()

    def get_serializer_class(
        self
    ) -> Union[JokeRetrieveUpdateDestroyModelSerializer, JokeSerializer]:
        if self.request.method == 'GET':
            return JokeSerializer

        return JokeRetrieveUpdateDestroyModelSerializer

    def retrieve(self, request, *args, **kwargs):
        serializer = self.get_serializer(
            JokeRetrieveService.execute(
                name=self.kwargs[self.lookup_url_kwarg]
            )
        )
        return Response(serializer.data)
