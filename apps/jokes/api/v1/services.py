import random
from typing import List, Optional
from apps.commons.services import ServiceAbstrac
from apps.jokes.models import Joke
from .repositories import JokeRepo
from .exceptions import PathParamNameIsNotValidAPIException
from .api_proxy import ChuckAPIProxy, DadAPIroxy
from .constants import ALLOWED_NAMES, NAME_CHUCK, NAME_DAD


class JokeRetrieveService(ServiceAbstrac):

    _ALLOWED_NAMES: list = ALLOWED_NAMES

    @classmethod
    def get_jokes_ids(cls) -> Optional[List[int]]:
        return sorted(JokeRepo.get_all_ids())

    @classmethod
    def get_joke_random(cls) -> Optional[Joke]:
        jokes_ids: list = cls.get_jokes_ids()
        if jokes_ids:
            random_id = random.choice(jokes_ids)
            return JokeRepo.get_joke_by_id(joke_id=random_id)

    @classmethod
    def get_joke_from_api_proxy(cls, name: str) -> Optional[Joke]:
        if name == NAME_CHUCK:
            return Joke(**ChuckAPIProxy.send_get_request())
        if name == NAME_DAD:
            return Joke(**DadAPIroxy.send_get_request())

    @classmethod
    def execute(cls, name: str = None) -> dict:
        if name:
            if name not in cls._ALLOWED_NAMES:
                raise PathParamNameIsNotValidAPIException()
            return cls.get_joke_from_api_proxy(name=name)

        return cls.get_joke_random()
